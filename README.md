#Terraform project for Siebel-Foundation.
Terraform version recommended: v0.11.10

Consists of Dev and staging environment resources which consist of:

 * EC2 instances for Web.
 * ALB for Web application.
 * IAM role and policies for EC2 instances.
 * Route 53 records
 * CodeDeployment app and Groups